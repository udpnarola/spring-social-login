package com.social.login.service.mapper;

import com.social.login.model.Authority;
import com.social.login.service.dto.AuthorityDto;
import org.springframework.stereotype.Component;

/**
 * @author puday
 */
@Component
public class AuthorityMapper {

  public AuthorityDto toDto(Authority authority) {
    AuthorityDto authorityDto = new AuthorityDto();
    authorityDto.setName(authority.getName());
    authorityDto.setDescr(authority.getDescr());
    return authorityDto;
  }
}
