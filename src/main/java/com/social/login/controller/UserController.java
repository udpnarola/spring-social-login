package com.social.login.controller;

import com.social.login.service.UserService;
import com.social.login.service.dto.UserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author puday
 */
@RestController
@RequestMapping("/api")
public class UserController {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

  private final UserService userService;

  public UserController(UserService userService) {
    this.userService = userService;
  }

  /**
   * @return current login User
   */
  @GetMapping("/users")
  public UserDto getUser() {
    LOGGER.info("Rest Api To Get Current Login User");
    return userService.getUser();
  }

}
