package com.social.login.service.mapper;

import com.social.login.model.User;
import com.social.login.service.dto.UserDto;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

/**
 * @author puday
 */
@Component
public class UserMapper {

  private final AuthorityMapper authorityMapper;

  public UserMapper(AuthorityMapper authorityMapper) {
    this.authorityMapper = authorityMapper;
  }

  public User toEntity(UserDto userDto) {
    User user = new User();
    return updateEntity(userDto, user);
  }

  public User updateEntity(UserDto userDto, User user) {
    user.setEmail(userDto.getEmail());
    user.setFirstName(userDto.getFirstName());
    user.setLastName(userDto.getLastName());
    user.setProvider(userDto.getProvider());
    user.setMiddleName(userDto.getMiddleName());
    user.setImageUrl(userDto.getImageUrl());
    return user;
  }

  public UserDto toDto(User user) {
    UserDto userDto = new UserDto();
    userDto.setId(user.getId());
    userDto.setEmail(user.getEmail());
    userDto.setFirstName(user.getFirstName());
    userDto.setLastName(user.getLastName());
    userDto.setProvider(user.getProvider());
    userDto.setAuthorities(user.getAuthorities().stream().map(authorityMapper::toDto).collect(Collectors.toList()));
    userDto.setMiddleName(user.getMiddleName());
    userDto.setImageUrl(user.getImageUrl());
    return userDto;
  }

}
