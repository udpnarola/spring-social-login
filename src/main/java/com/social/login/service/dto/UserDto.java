package com.social.login.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * @author puday
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto {

  private Long id;
  @NotBlank(message = "First Name Can't Be Null Or Empty")
  private String firstName;
  private String middleName;
  @NotBlank(message = "Last Name Can't Be Null Or Empty")
  private String lastName;
  @NotBlank(message = "Email Can't Be Null Or Empty")
  @Email(message = "Email Should Be Valid")
  private String email;
  private List<AuthorityDto> authorities = new ArrayList<>();
  private String imageUrl;
  @NotBlank(message = "Provider Can't Be Null Or Empty")
  private String provider;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getMiddleName() {
    return middleName;
  }

  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public List<AuthorityDto> getAuthorities() {
    return authorities;
  }

  public void setAuthorities(List<AuthorityDto> authorities) {
    this.authorities = authorities;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public String getProvider() {
    return provider;
  }

  public void setProvider(String provider) {
    this.provider = provider;
  }
}
