package com.social.login.config;

import com.social.login.dao.UserDao;
import com.social.login.model.User;
import java.util.Optional;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author puday
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {

  private final UserDao userDao;

  public CustomUserDetailsService(UserDao userDao) {
    this.userDao = userDao;
  }

  @Override
  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
    Optional<User> user = userDao.findByEmail(email);
    if (!user.isPresent()) {
      throw new UsernameNotFoundException("User not found!");
    }
    return UserPrincipal.create(user.get());
  }

  // This method is used by JWTAuthenticationFilter
  @Transactional
  public UserDetails loadUserById(Long id) {
    User user = userDao.findById(id).orElseThrow(
        () -> new UsernameNotFoundException("User not found with id : " + id)
    );
    return UserPrincipal.create(user);
  }
}
