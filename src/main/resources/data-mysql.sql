insert ignore into authority values('ROLE_USER','USER');
insert ignore into authority values('ROLE_ADMIN','ADMINISTRATOR');

insert ignore into system_user
(id,email,first_name,last_name,password,provider)
values(1,'admin@localhost','admin','admin','$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC','local');

insert into user_authorities (user_id,user_role)
SELECT * FROM (SELECT 1, 'ROLE_USER') AS tmp
WHERE NOT EXISTS (SELECT user_id, user_role FROM user_authorities WHERE user_id = 1 and user_role='ROLE_USER') LIMIT 2;

insert into user_authorities (user_id,user_role)
SELECT * FROM (SELECT 1, 'ROLE_ADMIN') AS tmp
WHERE NOT EXISTS (SELECT user_id, user_role FROM user_authorities WHERE user_id = 1 and user_role='ROLE_ADMIN') LIMIT 2;

