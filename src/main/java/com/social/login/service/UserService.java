package com.social.login.service;

import com.social.login.service.dto.UserDto;

/**
 * @author puday
 */
public interface UserService {

  UserDto getUser();

}
