package com.social.login.config;

/**
 * @author puday
 */
public class Constants {

  public static final String ROLE_USER = "ROLE_USER";
  public static final String ROLE_ADMIN = "ROLE_ADMIN";

  public static final String BEARER = "Bearer ";

}
