package com.social.login.service;

import com.social.login.service.dto.LoginDto;
import com.social.login.service.dto.UserDto;

/**
 * @author puday
 */
public interface AccountService {

  String registerSocialUser(UserDto userDto);

  String loginUser(LoginDto loginDto);
}
