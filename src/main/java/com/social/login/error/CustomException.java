package com.social.login.error;

/**
 * @author puday
 */
public class CustomException extends RuntimeException {

  public CustomException(String message) {
    super(message);
  }
}
