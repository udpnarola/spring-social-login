package com.social.login.service.impl;

import static com.social.login.config.Constants.ROLE_USER;

import com.social.login.dao.AuthorityDao;
import com.social.login.dao.UserDao;
import com.social.login.model.User;
import com.social.login.security.JwtTokenProvider;
import com.social.login.service.AccountService;
import com.social.login.service.dto.LoginDto;
import com.social.login.service.dto.UserDto;
import com.social.login.service.mapper.UserMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author puday
 */
@Service
public class AccountServiceImpl implements AccountService {

  private static final Logger LOGGER = LoggerFactory.getLogger(AccountServiceImpl.class);

  private final UserMapper userMapper;
  private final JwtTokenProvider jwtTokenProvider;
  private final UserDao userDao;
  private final AuthorityDao authorityDao;
  private final AuthenticationManager authenticationManager;

  public AccountServiceImpl(UserMapper userMapper, JwtTokenProvider jwtTokenProvider, UserDao userDao, AuthorityDao authorityDao, AuthenticationManager authenticationManager) {
    this.userMapper = userMapper;
    this.jwtTokenProvider = jwtTokenProvider;
    this.userDao = userDao;
    this.authorityDao = authorityDao;
    this.authenticationManager = authenticationManager;
  }

  @Override
  @Transactional
  public String registerSocialUser(UserDto userDto) {
    LOGGER.info("Method To Register Social User", userDto);
    User user;
    Optional<User> userOptional = userDao.findByEmail(userDto.getEmail());
    if (userOptional.isPresent()) {
      user = userMapper.updateEntity(userDto, userOptional.get());
      return jwtTokenProvider.generateToken(user);
    } else {
      user = userMapper.toEntity(userDto);
      authorityDao.findById(ROLE_USER).ifPresent(user::addAuthority);
      user = userDao.save(user);
      return jwtTokenProvider.generateToken(user);
    }
  }

  @Override
  @Transactional(readOnly = true)
  public String loginUser(LoginDto loginDto) {
    LOGGER.info("Method To Login User");
    Authentication authentication = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(loginDto.getEmail(), loginDto.getPassword())
    );
    SecurityContextHolder.getContext().setAuthentication(authentication);
    return jwtTokenProvider.generateToken(authentication);
  }
}
