package com.social.login.controller;

import com.social.login.service.AccountService;
import com.social.login.service.dto.LoginDto;
import com.social.login.service.dto.UserDto;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author puday
 */
@RestController
@RequestMapping("/api")
public class AccountController {

  private static final Logger LOGGER = LoggerFactory.getLogger(AccountController.class);

  private final AccountService accountService;

  public AccountController(AccountService accountService) {
    this.accountService = accountService;
  }

  /**
   * @param userDto to register social user
   * @return JWT token
   */
  @PostMapping("/social-users")
  public String registerSocialUser(@Valid @RequestBody UserDto userDto) {
    LOGGER.info("Rest Api To Register Social User", userDto);
    return accountService.registerSocialUser(userDto);
  }

  /**
   * @param loginDto to login user
   * @return JWT token
   */
  @PostMapping("/login")
  public String loginUser(@Valid @RequestBody LoginDto loginDto) {
    LOGGER.info("Rest Api To Login User", loginDto);
    return accountService.loginUser(loginDto);
  }
}
