package com.social.login.dao;

import com.social.login.model.User;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author puday
 */
public interface UserDao extends JpaRepository<User, Long> {

  Optional<User> findByEmail(String email);

}
