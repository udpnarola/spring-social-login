package com.social.login.security;

import com.social.login.config.UserPrincipal;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * @author puday
 */
@Component
public class SecurityUtil {

  public Long getCurrentLoginUserId() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    UserPrincipal userPrincipal = (UserPrincipal) auth.getPrincipal();
    return userPrincipal.getId();
  }
}
