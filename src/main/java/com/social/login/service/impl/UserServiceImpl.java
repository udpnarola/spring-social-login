package com.social.login.service.impl;

import com.social.login.dao.UserDao;
import com.social.login.error.CustomException;
import com.social.login.model.User;
import com.social.login.security.SecurityUtil;
import com.social.login.service.UserService;
import com.social.login.service.dto.UserDto;
import com.social.login.service.mapper.UserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author puday
 */
@Service
public class UserServiceImpl implements UserService {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

  private final UserDao userDao;
  private final UserMapper userMapper;
  private final SecurityUtil securityUtil;

  public UserServiceImpl(UserDao userDao, UserMapper userMapper, SecurityUtil securityUtil) {
    this.userDao = userDao;
    this.userMapper = userMapper;
    this.securityUtil = securityUtil;
  }

  @Override
  @Transactional(readOnly = true)
  public UserDto getUser() {
    LOGGER.info("Method To Get Current Login User");
    User user = userDao.findById(securityUtil.getCurrentLoginUserId())
        .orElseThrow(() -> new CustomException("Current Login User Is Not Available"));
    return userMapper.toDto(user);
  }
}
