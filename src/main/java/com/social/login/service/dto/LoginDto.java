package com.social.login.service.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class LoginDto {

  @NotBlank(message = "Email Can Not Be Null Or Empty")
  @Email(message = "Please Enter Valid Email Address")
  private String email;

  @NotBlank(message = "Password Can Not Be Null Or Empty")
  private String password;

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
}
